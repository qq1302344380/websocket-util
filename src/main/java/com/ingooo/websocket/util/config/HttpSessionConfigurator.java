package com.ingooo.websocket.util.config;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * Create by 丶TheEnd on 2019/10/9 0009.
 * @author Administrator
 */
public class HttpSessionConfigurator extends ServerEndpointConfig.Configurator {

    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        HttpSession httpSession = (HttpSession)request.getHttpSession();
        if (httpSession == null) {
            System.out.println("httpSession 是空的");
            return;
        }
        sec.getUserProperties().put(HttpSession.class.getName(), httpSession);
    }
}
