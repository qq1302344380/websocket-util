package com.ingooo.websocket.util.listener;

import org.springframework.stereotype.Component;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

/**
 * Create by 丶TheEnd on 2019/10/9 0009.
 * @author Administrator
 */
@Component
public class RequestListener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
        if (request.getRequestURI().equals("/websocket")) {
            request.getSession();
        }
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {

    }

    public RequestListener() {

    }
}
