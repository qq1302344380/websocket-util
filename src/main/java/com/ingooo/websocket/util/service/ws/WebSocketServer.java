package com.ingooo.websocket.util.service.ws;

import com.ingooo.websocket.util.config.HttpSessionConfigurator;
import com.ingooo.websocket.util.handler.WsSocketHandle;
import com.ingooo.websocket.util.model.WsSessionModel;
import com.ingooo.websocket.util.util.WsUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

/**
 * Create by 丶TheEnd on 2019/10/9 0009.
 * @author Administrator
 */
@Data
@Component
@ServerEndpoint(value = "/websocket", configurator = HttpSessionConfigurator.class)
public class WebSocketServer {

    private static WsSocketHandle wsSocketHandle;

    @Autowired(required = true)
    public void setWsSocketHandle(WsSocketHandle wsSocketHandle) {
        WebSocketServer.wsSocketHandle = wsSocketHandle;
    }

    /**
     * 连接建立成功回调方法
     * @param session
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        HttpSession httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
        try {
            if (httpSession == null) {
                session.close(new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, "非法访问"));
                return;
            }
            if (WsUtil.isOnlineByHttpSession(httpSession)) {
                session.close(new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, "当前已登录"));
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        WsUtil.addWsSocket(session);
        WsSessionModel wsSessionModel = WsUtil.getWsSessionModel(session);
        WsUtil.bindHttpSession(httpSession, session.getId());
        WebSocketServer.wsSocketHandle.onOpen(session, httpSession);

    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        WsUtil.removeByWsSession(session);
        WebSocketServer.wsSocketHandle.onClose(session);
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        WebSocketServer.wsSocketHandle.onMessage(message, session);
    }

    /**
     * 发生错误后调用的方法
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        WebSocketServer.wsSocketHandle.onError(session, error);
    }

}
