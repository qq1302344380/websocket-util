package com.ingooo.websocket.util.handler;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * Create by 丶TheEnd on 2019/10/9 0009.
 * @author Administrator
 */
public interface WsSocketHandle {

    /**
     * 连接建立成功的处理方法
     * @param session
     * @param httpSession
     */
    void onOpen(Session session, HttpSession httpSession);

    /**
     * 连接被管理的处理方法
     * @param session
     */
    void onClose(Session session);

    /**
     * 收到消息的处理方法
     * @param message
     * @param session
     */
    void onMessage(String message, Session session);

    /**
     * 发生异常时的方法
     * @param session
     * @param error
     */
    void onError(Session session, Throwable error);
}
