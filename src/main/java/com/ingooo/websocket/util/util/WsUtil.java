package com.ingooo.websocket.util.util;

import com.alibaba.fastjson.JSON;
import com.ingooo.websocket.util.model.HttpSessionModel;
import com.ingooo.websocket.util.model.WsSessionModel;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Create by 丶TheEnd on 2019/10/9 0009.
 * @author Administrator
 */
public class WsUtil {

    private static Map<String, HttpSessionModel> httpSessionMap = new ConcurrentHashMap<>(16);
    private static Map<String, WsSessionModel> wsSessionMap = new ConcurrentHashMap<>(16);

    /**
     * 给 WsSession 绑定 HttpSession
     * @param httpSession
     * @param wsSessionId
     */
    public static void bindHttpSession(HttpSession httpSession, String wsSessionId) {
        // 获取当前 wsSessionId 对应的 WsSessionModel
        WsSessionModel wsSessionModel = wsSessionMap.get(wsSessionId);
        // 通过参数中的 httpSession 对象和 WsSessionModel 实例化 httpSessionModel
        HttpSessionModel httpSessionModel = new HttpSessionModel(httpSession, wsSessionModel);
        // 给 wsSessionModel 绑定 httpSessionModel
        wsSessionModel.setHttpSessionModel(httpSessionModel);
        // 将 httpSessionModel 压入 httpSessionMap 中
        httpSessionMap.put(httpSession.getId(), httpSessionModel);
    }

    /**
     * 给 HttpSession 绑定 WsSession
     * @param session
     * @param httpSessionId
     */
    public static void bindWsSession(Session session, String httpSessionId){
        HttpSessionModel httpSessionModel = httpSessionMap.get(httpSessionId);
        WsSessionModel wsSessionModel = new WsSessionModel(session, httpSessionModel);
        httpSessionModel.setWsSessionModel(wsSessionModel);
        wsSessionMap.put(session.getId(), wsSessionModel);
    }

    /**
     * 添加 HttpSession
     * @param session
     */
    public static void addHttpSession(HttpSession session){
        httpSessionMap.put(session.getId(), new HttpSessionModel(session));
    }

    /**
     * 添加 WsSession
     * @param session
     */
    public static void addWsSocket(Session session) {
        wsSessionMap.put(session.getId(), new WsSessionModel(session));
    }

    /**
     * 通过 wsSessionId 移除当前登录信息
     * @param wsSessionId
     */
    public static void removeByWsSession(String wsSessionId){
        WsSessionModel wsSessionModel = wsSessionMap.get(wsSessionId);
        if (wsSessionModel == null) {
            return;
        }
        HttpSessionModel httpSessionModel = wsSessionModel.getHttpSessionModel();
        wsSessionMap.remove(wsSessionId);
        if (httpSessionModel == null || httpSessionModel.getSession() == null) {
            return;
        }
        String httpSessionId = httpSessionModel.getSession().getId();
        httpSessionMap.remove(httpSessionId);
    }

    /**
     * 通过 WsSession 移除当前登录信息
     * @param session
     */
    public static void removeByWsSession(Session session) {
        removeByWsSession(session.getId());
    }

    /**
     * 通过 httpSessionId 移除当前登录信息
     * @param httpSessionId
     */
    public static void removeByHttpSession(String httpSessionId) {
        HttpSessionModel httpSessionModel = httpSessionMap.get(httpSessionId);
        WsSessionModel wsSessionModel = httpSessionModel.getWsSessionModel();
        httpSessionMap.remove(httpSessionId);
        if (wsSessionModel == null || wsSessionModel.getSession() == null) {
            return;
        }
        String wsSessionId = httpSessionModel.getWsSessionModel().getSession().getId();
        wsSessionMap.remove(wsSessionId);
    }

    /**
     * 通过 HttpSession 移除当前登录信息
     * @param session
     */
    public static void removeByHttpSession(HttpSession session) {
        removeByHttpSession(session.getId());
    }

    /**
     * 通过 httpSessionId 获取 HttpSessionModel
     * @param httpSessionId
     * @return
     */
    public static HttpSessionModel getHttpSessionModel(String httpSessionId) {
        return httpSessionMap.get(httpSessionId);
    }

    /**
     * 通过 httpSession 获取 HttpSessionModel
     * @param httpSession
     * @return
     */
    public static HttpSessionModel getHttpSessionModel(HttpSession httpSession) {
        return getHttpSessionModel(httpSession.getId());
    }

    /**
     * 通过 wsSessionId 获取 HttpSessionModel
     * @param wsSessionId
     * @return
     */
    public static HttpSessionModel getHttpSessionModelByWsSession(String wsSessionId) {
        WsSessionModel wsSessionModel = wsSessionMap.get(wsSessionId);
        return wsSessionModel.getHttpSessionModel();
    }

    /**
     * 通过 wsSession 获取 HttpSessionModel
     * @param session
     * @return
     */
    public static HttpSessionModel getHttpSessionModelByWsSession(Session session) {
        return getHttpSessionModelByWsSession(session.getId());
    }

    /**
     * 通过 wsSessionId 获取 WsSessionModel
     * @param wsSessionId
     * @return
     */
    public static WsSessionModel getWsSessionModel(String wsSessionId) {
        return wsSessionMap.get(wsSessionId);
    }

    /**
     * 通过 wsSession 获取 WsSessionModel
     * @param session
     * @return
     */
    public static WsSessionModel getWsSessionModel(Session session) {
        return getWsSessionModel(session.getId());
    }

    /**
     * 通过 httpSessionId 获取 WsSessionModel
     * @param httpSessionId
     * @return
     */
    public static WsSessionModel getWsSessionModelByHttpSession(String httpSessionId) {
        HttpSessionModel httpSessionModel = httpSessionMap.get(httpSessionId);
        return httpSessionModel.getWsSessionModel();
    }

    /**
     * 通过 httpSession 获取 WsSessionModel
     * @param session
     * @return
     */
    public static WsSessionModel getWsSessionModelByHttpSession(HttpSession session) {
        return getWsSessionModelByHttpSession(session.getId());
    }

    /**
     * 获取当前在线总数
     * @return
     */
    public static long getOnlineTotal(){
        return wsSessionMap.size();
    }

    /**
     * 通过当前 HttpSession 判断对应的 WebSocket 是否在线
     * @param httpSessionId HttpSession 的 sessionId
     * @return
     */
    public static boolean isOnlineByHttpSession(String httpSessionId) {
        HttpSessionModel httpSessionModel = httpSessionMap.get(httpSessionId);
        if (httpSessionModel == null) {
            return false;
        }
        return httpSessionModel.getWsSessionModel() != null;
    }

    /**
     * 通过当前 HttpSession 判断对应的 WebSocket 是否在线
     * @param session HttpSession 对象
     * @return
     */
    public static boolean isOnlineByHttpSession(HttpSession session) {
        return isOnlineByHttpSession(session.getId());
    }

    /**
     * 通过 wsSessionId 判断对应的 WebSocket 是否在线
     * @param wsSessionId
     * @return
     */
    public static boolean isOnlineByWsSession(String wsSessionId) {
        return wsSessionMap.get(wsSessionId) != null;
    }

    /**
     * 通过 session 判断对应的 WebSocket 是否在线
     * @param session
     * @return
     */
    public static boolean isOnlineByWsSession(Session session) {
        return isOnlineByWsSession(session.getId());
    }

    /**
     * 给全部在线用户发送文本消息
     * @param msg
     */
    public static boolean sendMsg(String msg){
        boolean result = true;
        for (Map.Entry<String, WsSessionModel> entry : wsSessionMap.entrySet()) {
            try {
                entry.getValue().getSession().getBasicRemote().sendText(msg);
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
            }
        }
        return result;
    }

    /**
     * 给全部在线用户发送JSON文本格式的对象
     * @param object
     * @return
     */
    public static boolean sendMsg(Object object) {
        return sendMsg(JSON.toJSONString(object));
    }

    /**
     * 给某个 Session 发送文本消息
     * @param session
     * @param msg
     * @return
     */
    public static boolean sendMsgOnly(Session session, String msg){
        boolean result = true;
        try {
            session.getBasicRemote().sendText(msg);
        } catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    /**
     * 给某个 Session 发送JSON文本格式的对象
     * @param session
     * @param object
     * @return
     */
    public static boolean sendMsgOnly(Session session, Object object) {
        return sendMsgOnly(session, JSON.toJSONString(object));
    }
}
