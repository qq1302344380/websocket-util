package com.ingooo.websocket.util.model;

import lombok.Data;

import javax.servlet.http.HttpSession;

/**
 * Create by 丶TheEnd on 2019/10/9 0009.
 * @author Administrator
 */
@Data
public class HttpSessionModel {

    private HttpSession session;
    private WsSessionModel wsSessionModel;

    public HttpSessionModel() {
    }

    public HttpSessionModel(HttpSession session) {
        this.session = session;
    }

    public HttpSessionModel(HttpSession session, WsSessionModel wsSessionModel) {
        this.session = session;
        this.wsSessionModel = wsSessionModel;
    }
}
