package com.ingooo.websocket.util.model.message;

import lombok.Data;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@Data
public class BasicMessage {
    private String code;
    private String msg;
    private Object data;

    public BasicMessage() {
    }

    public BasicMessage(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
