package com.ingooo.websocket.util.model;

import lombok.Data;

import javax.websocket.Session;

/**
 * Create by 丶TheEnd on 2019/10/9 0009.
 * @author Administrator
 */
@Data
public class WsSessionModel {

    private Session session;
    private HttpSessionModel httpSessionModel;

    public WsSessionModel() {
    }

    public WsSessionModel(Session session) {
        this.session = session;
    }

    public WsSessionModel(Session session, HttpSessionModel httpSessionModel) {
        this.session = session;
        this.httpSessionModel = httpSessionModel;
    }
}
