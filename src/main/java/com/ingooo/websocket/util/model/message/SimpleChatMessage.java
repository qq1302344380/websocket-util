package com.ingooo.websocket.util.model.message;

import lombok.Data;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@Data
public class SimpleChatMessage {

    private String from;
    private String type;
    private String msg;
    private Object data;

    public SimpleChatMessage() {
    }

    public SimpleChatMessage(String from, String type, String msg) {
        this.from = from;
        this.type = type;
        this.msg = msg;
    }

    public SimpleChatMessage(String from, String type, String msg, Object data) {
        this.from = from;
        this.type = type;
        this.msg = msg;
        this.data = data;
    }
}
